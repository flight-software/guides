---
output: pdf_document
---

# LASSI Gomspace AX100 Communications Stack

## Repositories
[gr-ax100](https://gitlab.engr.illinois.edu/cubesat/ground-station/gr-ax100) (ground side Gnuradio flowchart, gnu radio block library, monolithic client and server tool)  
[ax100](https://gitlab.engr.illinois.edu/cubesat/flight-software/radios/ax100) (client and server tool for cdh/satellite side)

## Usage
To start the cdh program as a daemon, run `rg-d [ax100 serial file]`. The program starts as a client and runs continuously, it can act as a server if requested by the ground. For testing, use `rg-d [ax100 device link] <filename>` to start the program as a server, it will then turn enter client mode after transmitting the file.

Use `ax100_monolith <cmd> [filename]` to run the ground program. Available commands:

* `--client-once` run a single client session. Program will terminate after receiving a file or encountering a timeout.
* `--client-inf` run a series of client sessions. Program will start another client session after receiving a file or encountering a timeout, running indefinitely until terminated.
* `--uplink-file <file>` uplink a regular file to the client.
* `--upload-schedule <file.sched>` upload a schedule file to the client. File extension must be `.sched`
* `--upload-shell-cmd <file>` upload a shell-cmd file to be executed by cdh. Maximum length of file is limited as it needs to fit in a single packet, give file a `.sched` extension and use `--upload-schedule` as a workaround to send larger scripts. At this time, upload shell command also does not send the packet again if it is missed. Therefore, it is recommended to try again manually or through a script or use upload schedule.
* `--init-file-download <file>` initiate a file download. The file location needs to refer to a location on the cdh side. relative paths should work, but have not been tested in detail. The program will automatically start a single client session after sending the request to receive the file.
* `--req-beacon` request a beacon from the ground, useful for debugging

Run `./ax100_uplink -h` for a list of these commands. Consult the AX100 command grammar specification for some details on the structure of the packets for these requests and how the cdh side will process them.

For both sending and receiving files, use the new-and-improved ax100_merged GNUradio graph with GNURadio so the ground program can use the SDR. For convenience, a python script that can be used via ssh is provided, which can currently be used with `python gr-ax100/top_block_nowindows_GR38.py`. This script uses python3 and Gnuradio 3.8  and no longer depends on X windows support. Note that the updated build also expects Gnuradio 3.8, so make sure to use this version if you plan to build on a new system. The older script `top_block_nowindows_06112021.py` for python2 and GNURadio 3.6 is still present, but it does not include changes to improve reliability and would need to be updated.

#### Ground operations caveats
Due to limitations to the command grammar and the libcrap protocol, it is important to avoid some scenarios that can lead to recoverable, but annoying malfunctions:

* Avoid requesting non-existent files and files with a size of zero bytes. rg-d will be unable to fulfill the request and the ground may continuously keep requesting the file until it is manually stopped. This can lead to us wasting time that could be used transmit meaningful data.
* Due to issues outside of the control of the radio stack (hm-d failing to respond to rg-d requesting beacons), rg-d may take a while to receive a valid beacon. Please do not be surprised if receiving a beacon takes a while or if several beacons arrive when requesting one. If there is no hm-d response after 5 beacon requests, an all-zero beacon may be sent to the ground. This is not believed to be long or common enough to pose a serious risk to file transmissions.
* If we request a file downlink while rg-d is waiting for missed package requests, the downlink will only be processed after the file transmission has ended (i.e. the client gives up on it). This can lead to the file being requested and rg-d sending it multiple times, which can waste time in a pass.  
This should not happen unless a file downlink is requested while rg-d has partially received a file, which can only happen if the program is operated manually and the operator makes a mistake or as part of a script where a file downlink is still requested after the uplink crashes, making this a very rare scenario.  
libcrap is believed to avoid similar issues if a file is starting to get sent after parts of another file have already been sent.
* If both sides end up acting as servers, the ground libcrap server will shut down to avoid having two servers both being unable to complete their transmissions and will become a client. However, having two servers is currently believed to not be possible with the ground interface, unless we deliberately quit a program or it crashes and we then start a server.

### Building & Encryption key
Both ground and cdh programs can be built using cmake. Building on the ground side can cause issues, but this approach should work: First, build and install the library for GNURadio by building on the repository level and run `LD_LIBRARY_PATH=/usr/local/lib && sudo install libcrap.so /usr/local/lib && sudo ldconfig` to install it. Then, build in the lib/ folder with `BUILD_UTILS=1` set. For the CAPSAT mission, a key defined at build time will be used. Since the build will fall back to an an all-zero key if no key is supplied, it is imperative to build with the command for the defined key to avoid using an all-zero key or having mismatching keys, resulting in both sides being unable to decrypt messages.  
The command to build with the key can be found in both repositories as well as on Sharepoint. The first build command is for building on the ground side (hence it starts with `BUILD_UTILS=1`), the second command is for the rg-d side.  
As long as the key does not get lost, there is little potential for this causing issues. In case there is an apparent mismatch in keys, it is likely that one of both sides was built using an all-zero case. In that case, switching what the ground is doing (i.e. using the defined key if accidentally using the zero key on the ground or switching to the zero key on the ground if transmission does not work, suggesting that rg-d is using a zero key) should fix this issue.

### Program behavior
The ground sends files and commands to the cdh. The satellite responds with tracking beacons (generated by the AX100 radio itself), regular beacons (from the health monitor) and files and rarely responses to commands. Both sides produce error and debug messages, which can be accessed through `sudo ls /var/log/messages`.  
File names will be sanitized, i.e. we can send or request files from any location on disk using both absolute and relative paths. Transmitted files will be put in `/var/log/cdh/uplink`.  
Schedule files are required to have the `.sched` file extension, otherwise they will not be executed.

#### Caveats
The client/server terminology is misleading as the client is used to receives files, possibly continuously, while the server is used to send one file each.

#### Picocom
The satellite picocom interface can be reached by running `picocom <port> -b 500000`. Use this interface to set the right configuration paprameters as described in the AX100 configuration parameters document and make sure to write them to persistent memory.  
Use `log print csp debug` to enable csp debug messages, which are very useful to find bugs in the AX100, Make sure to reset the value afterwards (Not sure what the command is, might be `log print csp err`).

#### Implementation limitations: 
File paths may not be longer than `CRAP_MAX_PATH_LEN`   
We do not support non-ascii file names. Non-ascii files are supported

#### Encryption
All uplink communications are encrypted. Downlink file transmissions are encrypted while downlink error packets, beacons and necessarily tracking beacons are not encrypted.

#### CAPSAT Beacon handling
CAPSAT beacons are currently sent when requested by the ground or when an error in the radio protocol happens. Beacons are saved to a file in the directory where the ground program is executed. The file name is the date and time when the beacon was transmitted.

#### Tracking beacon handling
**Note that we have disabled the tracking beacon feature for CAPSAT. The tracking beacon can cause issues with the radio sending when we don't want it to during testing if TX\_INHIBIT in the radio configuration has not been set properly.**  
There currently is rudimentary support for AX100 beacon handling (this refers to the telemetry or tracking beacons of the radio itself, not the beacon of a satellite mission). The AX100 starts sending a tracking beacon every 10 seconds if there has not been any tx/rx traffic for 4800 seconds (e.g. due to a bus failure). The beacon contains the 64 byte AX100 telemetry table as described in the AX100 manual. The ax100 ground client will save the latest tracking beacon to 'telem.bin' in the current directory. Since the AX100 packet decoder expects packets to have a csp header while the tracking beacon does not have one, the first 10 bytes of the beacon will be cut off. The beacon data is of low priority, but ist would still be useful to be able to recover the entire beacon data for analyzing failures. The format of the AX100 beacon is described in table 5.5 of the AX100 manual.

### Useful commands and links
[Command grammar document](https://gitlab.engr.illinois.edu/cubesat/flight-software/guides/blob/master_staging/docs/ax100/ax100_comm_spec.pdf)  
`https://cubesat-vm.ae.illinois.edu:8006/#v1:0:=qemu%2F100:4::::8::` access egse VM via Proxmox  
`ssh \<netid\>@egse-vm.ae.illinois.edu -p 5023` connect to VM via SSH  
`/dev/serial/by-id/usb-FTDI_TTL232RG-VREG3V3_FT1I6923-if00-port0` serial port for picocom
`/dev/serial/by-id/usb-FTDI_TTL232RG-VREG3V3_FT1I6D9J-if00-port0` serial port for sending/receiving packets    
`ps -eo 'tty,pid,comm'` find out which daemons are running  
`kill -9 <pid>` kill a daemon

## Supporting information

#### Sources for AX100 protocol overhead
* Internal overhead: 47 bytes of overhead per data packet, including encryption, so we have 208B of data when sending a 255B packet. When using the libcrap protocol, which is used for regular file transmissions, the effective MTU is 192 bytes.
* FITF Packets. I'm not sure yet how many there should be. These are around 160 bytes IIRC.  
* Lost packet recovery. We'll need to send lost packet requests (16B raw size) and lost packet responses (212B raw size), followed by retransmitting the lost packets.  
* Timeouts due to waiting for missed packet requests/responses. In recent code changes I have sought to minimize these, so hopefully they will only be a considerable issue if we have more packet loss then expected.
* Successful transmission packet: There should be 4 of them @ 255 bytes at the end of every file transmission
* Error packets and beacons: Will be sent from time to time and before some libcrap file transmissions, depending on how transfer was initiated. Size unknown, but fits in 1 packet.
* Waiting times to account for keyup/key down delay. Less of an issue for large files.

As the file size increases, the size of the overhead should approach 63B for 192B of data in addition to packets needing to be resent. I can do some testing to get if you're interested, but I would estimate that overhead could take 25-30% of the MTU for large files.

## Additions to AX100 Command grammar  
These notes represent the current behavior of the implementation and should be added to the command grammar document.  

##### File naming conventions  
The file name field may only contain a file name as opposed to a file path indicating an absolute or relative directory, except for schedule files.  
A schedule file is required to have a file name ending with ".sched". While there is also an op-code for schedule files, the receiving side should interpret a file as a schedule file if and only if it has the ".sched" file extension.  
While error codes for nonzero shell command return values are defined, they will never actually be sent as they are of limited utility.

## Cross-mission considerations
The AX100 Communications stack was designed with the CAPSAT mission in mind. It can also be adapted to be used with other missions, such as the SPACEICE mission. There will be some changes necessary to support using the radio stack across missions:

* In the AX100 configuration and the GNURadio flowchart, the uplink and downlink frequency needs to be adjusted for the new mission.
* If the beacon size changes, this must be adjusted in the rg-d program. If the ground is to do advanced beacon handling, this must also be handled on the ground side. Similarly, if there are changes to the beacon handling on the health monitor side, such as changes to the health monitor RPC port, additional defines will need to be updated in the rg-d program.
* Based on the UDP communications used on the ground side between GNURadio and the ground program, it is not recommended to try to communicate with two radios at once, which is likely to produce issues unless changes are made to both GNURadio and the ground program.
* Depending on how the file used to communicate with the AX100 is used, the other AX100 will have a different serial file name and the name will need to be updated.
* (Recommended) A different encryption key should be used to improve security. This must be done for both the ground and the rg-d programs.
* (Optional) The CSP protocol node and port numbers may be updated, which would involve the AX100 configuration, ground and rg-d programs, but as long as a different radio frequency is used this step may not be necessary. If these changes are not done, the `freq` parameter in tables 1 (uplink) and 5 (downlink) is the only parameter in the AX100 configuration that needs to differ between missions.
* The following directories need to exist on both systems:  
/var/log/cdh/  
/var/log/cdh/uplink  
/var/log/cdh/crap\_chunk\_store\_ground/  
/var/log/cdh/crap\_chunk\_store\_cdh/  
If the libcrap client has the permission to create these directories, they will be created by it, so if everything works as expected creating these directories should not be necessary. The libcrap server used for uplink does not need to write any files. In the past we encountered permissions issues on the ground side. While recent patches should have fixed this issue, in case this still happens it might help to create the directories manually, otherwise it could be necessary to run the ground program with elevated permissions.

### Configuring the AX100
To update the AX100 to the right configuration, the picocom interface should be used by running `picocom <serial file> -b 500000`. It is important that not only the main configuration stored in RAM is updated, but that the backup configurations (both the boot file and alternative boot file) stored on disk is also correct in case of memory corruption, reboots or crashes. This needs to be done for the boot file tables 0, 1 and 5 and the alternative boot file tables 24, 25, 29. Information on the parameters to be used and how to configure them is contained in the AX100 configuration parameters document.  
(It should just be config unlock followed by param save 0 0 param save 1 1 param save 5 5 param save 0 24 param save 1 25 param save 5 29). config verify_default can be used to verify that the two disk tables (e.g. disk tables 5 and 29) are matching.
