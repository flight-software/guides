---
docid: 
title: AX100 Protocol Definition
subtitle: 
pi: 
engineeringlead: 
author: Dillon Hammond
revision: C
date: 2019-09-27
changes: |
    * Updated Remaining Issues
...

# Justification
This document is the underlying protocol that we will use for data packets on
the AX100 radios, and possibly others if it is deemed useful.
This protocol is toolchain agnostic and affects only the underlying packet data.

# How to Read this Document
This document is organized into an explanation of how the protocol works,
followed by a series of op-codes followed by their packet structures. Keep in
mind that:

* down/up is always from the perspective of the ground (downlink is from satellite to ground, uplink is from ground to
satellite).

# Configurable Values
* Total Useable Packet Length

  This is affected by the communication protocol used to transmit the data. This
  protocol does not care about the minimum length of packets (to a reasonable
  degree) but larger amounts of usable space will increase quality of life. If a
  variable length field is encountered, it is constrained by the Total Useable
  Packet Length.

# Protocol Overview
The core of this protocol is that it allows ground operators to control the
satellite by uploading a schedule file. This schedule file consists of a series
of shell commands that are then executed by the satellite. The ground can also
explicitly request to: downlink a file, uplink a file, and directly access the
shell. There is also a focus with time-based synchronization baked into the
packets of the protocol, to ensure consistency between satellite and ground.

# Schedule File
A schedule file consists of two logical sections, each consisting of single
lines of bash commands:

1. Immediate Commands

These are commands that will be run as soon as the schedule file successfully
uploads.

2. Scheduled Commands

These commands are written as _at_ commands. When these commands are run, _at_
will schedule them to run at the designated time. See the linux _at_ command for
more details.

# Op-Codes

## Responses

If any of the following op-codes (except Beacon) fails, a NACK response will be sent of the form:

| Field | Encoding | Width (Bytes) |
| :---: | :------: | :-----------: |
| Op-Code | Binary | 1 |
| Custom Error Code | Binary | 4 |
| Current value of `errno` | Binary | 4 |
| Beacon | Binary | M |

Error codes are defined per op-code.

## Upload Schedule

Packet Contents:

| Field | Encoding | Width (Bytes) |
| :---: | :------: | :-----------: |
| Op-Code (0x01) | Binary | 1 |
| Timestamp | Binary | 4 |
| Filename | ASCII | N |
| '\0' | ASCII | 1 |

Execution:

1. Sets hardware and software clock
2. Enters a file transfer session
3. Execute schedule file

Custom Error Codes:

| Value | Meaning |
| :---: | :-----: |
| `0x00000001` | Other/Unknown |
| `0x00000002` | Failed to set HW clock |
| `0x00000003` | Failed to set SW clock |
| `0x00000004` | Filename did not have an NULL terminator |

## Downlink File

Packet Contents:

| Field | Encoding | Width (Bytes) |
| :---: | :------: | :-----------: |
| Op-Code (0x02) | Binary | 1 |
| Timestamp | Binary | 4 |
| Filename | ASCII | N |
| '\0' | ASCII | 1 |

Execution:

1. Sets hardware and software clock
2. Enters a file transfer session

Custom Error Codes:

| Value | Meaning |
| :---: | :-----: |
| `0x00000001` | Other/Unknown |
| `0x00000002` | Failed to set HW clock |
| `0x00000003` | Failed to set SW clock |
| `0x00000004` | Filename did not have an NULL terminator |

## Uplink File

Packet Contents:

| Field | Encoding | Width (Bytes) |
| :---: | :------: | :-----------: |
| Op-Code (0x03) | Binary | 1 |
| Timestamp | Binary | 4 |
| Filename | ASCII | N |
| '\0' | ASCII | 1 |

Execution:

1. Sets hardware and software clock
2. Enters a file transfer session

Custom Error Codes:

| Value | Meaning |
| :---: | :-----: |
| `0x00000001` | Other/Unknown |
| `0x00000002` | Failed to set HW clock |
| `0x00000003` | Failed to set SW clock |
| `0x00000004` | Filename did not have an NULL terminator |

## Shell Execution

Packet Contents:

| Field | Encoding | Width (Bytes) |
| :---: | :------: | :-----------: |
| Op-Code (0x04) | Binary | 1 |
| Timestamp | Binary | 4 |
| Shell Command | ASCII | N |
| '\0' | ASCII | 1 |

Execution:

1. Sets hardware and software clock
2. Executes the command in a `/bin/sh` shell

Custom Error Codes:

| Value | Meaning |
| :---: | :-----: |
| `0x00000001` | Other/Unknown |
| `0x00000002` | Failed to set HW clock |
| `0x00000003` | Failed to set SW clock |
| `0x00000004` | Shell command did not have an NULL terminator |
| `0x00000005` | Shell command failed |

## Beacon

Packet Contents:

| Field | Encoding | Width (Bytes) |
| :---: | :------: | :-----------: |
| Op-Code (0x05) | Binary | 1 |
| Beacon Data | Binary | M |

Execution:

1. Sends an unencrypted beacon, there is no response

Custom Error Codes: N/A

# Remaining Issues
* Beacon length is not yet known
