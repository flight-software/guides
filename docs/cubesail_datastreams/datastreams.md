---
docid: 
title: CubeSail Datastreams
subtitle: 
pi: 
engineeringlead: 
author: Dillon Hammond
revision: A
date: 2019-02-28
changes: |
    N/A.
...

# What are Data Streams?
Data streams were the original solution to logging daemon-specific information to disk at specific intervals. They have since been replaced for reasons described later.

# Over Arching Concept
The concept if datastreams was that any daemon could have any number of datastreams and that in each of these datastreams, there is data logged as a CSV in a "command-response" format. That is, data consists of the raw command to gather the data, and then the raw data itself.
Every datastream has a "full" cadence and "min" cadence varient. These are analogous to "fast" and "slow" logging speeds. Full cadence files are stored in `/var/log/cdh/full_cadence` and min cadence in `/var/log/cdh/min_cadence`.
Each pair of full-min datastreams comes from a single "sub system" which has a unique numeric id.

# Format

## File Name
A datastream file has a name of the form:

```
streamid_YYYYmmddHHMMSS
```

Where the streamid is an 8 character hex string (padded with 0's as appropriate). It is calculated as so:

```
XXYYZZZZ
```

Where `XX` is the subsystem id being logged, `YY` is the command length, and `ZZZZ` is the response length.

## File Contents
Data is logged in the format

|        Field       |      Encoding    | Width (Bytes) |
| ------------------ | ---------------- | ------------- |
| Timestamp (YYYYmmddHHMMSS) | ASCII  | 14 |
| ","                        | ASCII  |  1 |
| Raw Command                | Binary |  N |
| ","                        | ASCII  |  1 |
| Raw Response               | Binary |  M |
| "\\n"                      | ASCII  |  1 |

# Limitations
There are several flaws in this system (which is why it has been replaced).

1. Two different datastreams that have the same subsystem id and same command and response length *may* log to the same file.

2. Most data does not logically match the "command-response" format and as such it is unergonomic for developers.

3. It mixes binary and ASCII data in the same file, this makes viewing/parsing weird.

4. The data logging scheme uses much more data than should be necessary, requiring compression on the satellite before reasonable downlinking.

5. Subsystem IDs are not intuitive.

6. Command data is limited to 2^8 Bytes.

# CubeSail Data Streams

Unfortunately despite planning for more, on the current revision of CubeSail software only powerboard actually logs to its datastreams. This may be adjusted in future updates if communications is established (and this document will be updated).

## Powerboard Daemon (`pb-d`)

### Battery Balancer Status

| Stream ID | Subsystem ID | Command Length | Response Length |
| :-------: | :----------: | :------------: | :-------------: |
| 01030001  |     01       |       03       |     0001        |

- The Command is:

| `uint16_t` |
| :--------: |
| `0x0004`   |

- The Response is:

| `uint8_t` |
| :-------: |
| Battery Balancer Status |

### Battery Voltage

| Stream ID | Subsystem ID | Command Length | Response Length |
| :-------: | :----------: | :------------: | :-------------: |
| 01030008  |     01       |       03       |     0008        |

- The Command is:

| `uint16_t` |
| :--------: |
| `0x0005`   |

- The Response is:

| `uint8_t[8]` |
| :----------: |
| Battery Voltage (Uncalibrated) |

### Battery Temperature

| Stream ID | Subsystem ID | Command Length | Response Length |
| :-------: | :----------: | :------------: | :-------------: |
| 01030008  |     01       |       03       |     0008        |

- The Command is:

| `uint16_t` |
| :--------: |
| `0x0006`   |

- The Response is:

| `uint8_t[8]` |
| :----------: |
| Battery Temperature (Uncalibrated) |

### Hotswap Current

| Stream ID | Subsystem ID | Command Length | Response Length |
| :-------: | :----------: | :------------: | :-------------: |
| 0103001C  |     01       |       03       |     001C        |

- The Command is:

| `uint16_t` |
| :--------: |
| `0x000B`   |

- The Response is:

| `uint8_t[28]` |
| :-----------: |
| Hotswap Currents (Uncalibrated) |

### Hotswap Fault Status

| Stream ID | Subsystem ID | Command Length | Response Length |
| :-------: | :----------: | :------------: | :-------------: |
| 01030002  |     01       |       03       |     0002        |

- The Command is:

| `uint16_t` |
| :--------: |
| `0x000C`   |

- The Response is:

| `uint8_t[2]` |
| :----------: |
| Hotswap Fault Status of Each Board |

### Solar Charger Status

| Stream ID | Subsystem ID | Command Length | Response Length |
| :-------: | :----------: | :------------: | :-------------: |
| 01030001  |     01       |       03       |     0001        |

- The Command is:

| `uint16_t` |
| :--------: |
| `0x000F`   |

- The Response is:

| `uint8_t` |
| :-------: |
| Bitmask of Fault and Charge Status |

