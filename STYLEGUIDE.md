# The SatDev Flight Software Official Styleguide
This guide will cover C and Cmake related files, and may be eventually extended to scripts and the like.

# C
Sections:
1. [General Editing Environment](#general-editing-environment)
2. [Macros](#macros)
3. [Structs](#structs)
4. [Enums](#enums)
5. [Functions](#functions)
6. Variables
7. [Header Files](#header-files)
8. Source Files
9. Comments

## General Editing Environment
[go back](#c)
- Text files should **always** be edited with `\n` for newlines (**not** `\r\n`)
- Text files should end in an empty line
- Text files should **not** contain tab `\t` characters. Use 4 spaces for a tab.
- You can ignore 4 spaces for a tab in limited scenarios if the visual alignment makes something easier to read.
- Line length has a soft limit of 100 characters. Hard limit at 120.

## Macros
[go back](#c)

All Macros will use UPPER_SNAKE_CASE for their name. Names should be prefixed meaningfully to prevent conflicts.
```C
#define LIBUDP_AMAZING_MACRO ... // Library
#define POWERBOARD_BETTER_MACRO ... // Program
```

There are several subcases:
1. [Numeric Constant Macros](#numeric-constant-macros)
2. [String Constant Macros](#string-constant-macros)
3. [Function Macros](#function-macros)
4. [X Macros](#x-macros)

### Numeric Constant Macros
[go back](#macros)

Numeric Constant Macros are of the form:
```C
#define MACRO_NAME [TYPE_CONSTRUCTOR](NUMERIC)
```
Numeric Constant Macros should always surround their Numeric in parentheses. The Type Constructor is an optional component that should be used when the type of the Numeric is meaningful. Type Constructors are defined [here, scroll to Macros for Integer Constant Expressions](http://www.man7.org/linux/man-pages/man0/stdint.h.0p.html#Macros-for-Integer-Constant-Expressions) or in `$ man stdint.h`. See the examples for clarification on usage. Numeric Constant Macros used for bitmasking operations should **always** have the Type Constructor.

It is also important to consider whether your Numeric should be written as base 8 (octal), 10, or 16(hex). When writing in hex, use UPPERCASE for letters.

For sequential, related Numeric Constant Macros, consider aligining their Numerics in a visually appealing manner.

Examples:
```C
#define LIBDAEMON_FILE_PERMISSIONS (0700) // Leading 0 indicates octal. There is no meaningful type.
#define LIBUDP_MAX_PACKET_SIZE (999) // Base 10. No meaningful type.
#define MONITOR_SYNC_BYTE UINT8_C(0xFF) // Base 16. Meaningful type, needs to be 1 unsigned byte.
```

### String Constant Macros
[go back](#macros)

String Constant Macros are of the form:
```C
#define MACRO_NAME ("STRING")
```
Remember that `sizeof(MACRO_NAME) == strlen(MACRO_NAME) + 1`.

### Function Macros
[go back](#macros)

Function Macros should not exist in this code base. If you find yourself writing a macro that is essentially just a function, stop and reevalute your use case. If you *really really* think your use case is justifiable, make a detailed comment explaining why.

### X Macros
[go back](#macros)

An X macro takes the form (this example taken from an older incarnation of `libdaemon.h/c`):
```C
// libdaemon.h
#define DAEMON_TABLE \
    X(PBd, "pb-d")   \
    X(HMd, "hm-d")

#define X(a, b) a,
enum DAEMON {
    DAEMON_TABLE
};
typedef enum DAEMON DAEMON;
#undef X
// libdaemon.c
int get_daemon_binary_name(DAEMON d, char** daemon_)
{
#define X(a, b)       \
    case a:;          \
        *daemon_ = b; \
        break;
    
    switch(d) {
        DAEMON_TABLE
        default:;
            return EXIT_FAILURE;
    }
#undef X
    return EXIT_SUCCESS;
}
```
[Here is a link](https://godbolt.org/z/0_c21n) containing the expansion of these macros so that the use case becomes clear.
Essentially, an X Macro is a macro that can use other macros as a sort of "argument" to its expansion to produce consistent usage of certain names across usage sites. It is almost always preferable to avoid these, as they tend to confuse code editors and users when they try to find the source definition of your X Macro. If you choose to use these, a comment should be included that gives an example of the result of this macro.

## Structs
[go back](#c)

All Struct names should be written in `lower_snake_case`. All Struct names should be prefixed meaningfully to avoid naming conflicts and end in `_fs` (for Flight Software). There are 3 parts in creating a new Struct:
- Defining the members
- Creating the typename
- Choosing a [Typical Struct](#typical-structs) or a [Packed Struct](#packed-structs)

Defining the members and creating a typename are done as 2 steps in order to produce better error messages from compilers.

Example
```C
struct libudp_message_fs { // Step 1
    char* buffer;
    struct libudp_message_fs* next_msg;
};
typedef struct libudp_message_fs libudp_message_fs; // Step 2
```
Notice that the keyword `struct` is still required when declaring fields.

Structs should be initialized using the braced initializer syntax where appropriate, with the fields assigned in the same order as they are declared.

Example
```C
libudp_message_fs* m = ...;
char* b = ...;
libudp_message_fs message = {.buffer = b, .next_msg = m};
// or if there are many fields
libudp_message_fs message = {
    .buffer = b,
    .next_msg = m
};
// or if you are reassigning a struct variable (notice the cast)
message = (libudp_message_fs) {
    .buffer = b,
    .next_msg = m
};
```

There are 2 specific cases to consider when creating a new Struct:

### Typical Structs
[go back](#structs)

Typical Structs are what you should normally use, and all the information is present in [Structs](#structs).

### Packed Structs
[go back](#structs)

Packed Structs should only be chosen if the Struct you are writing will be used in serialized communications where space is at a premium. Generally, this only applies to data that will be transmitted back to ground.

Packed Structs have an additional GCC attribute attached and a modified name:
```C
struct monitor_status_packed_fs {
    short info;
    int some_flag;
    char* buffer;
} __attribute__((packed));
typedef struct monitor_status_packed_fs monitor_status_packed_fs;
```
This tells the compiler that the Packed Struct should not have any alignment padding inserted between fields or after the Packed Struct in memory, that is: `sizeof(monitor_status_packed_fs) == sizeof(short) + sizeof(int) + sizeof(char*)`. This is convenient for transmitting the Packed Struct over radio because we minimize the number of bits required, however it causes the compiler to generate either additional instructions to move field around in memory during access or to generate unaligned access instructions which are typically several orders of magnitude slower than normal instructions.

If a Struct needs to be passed around in code but also transmitted in a medium where packing makes sense, you should have two definitions of the struct, one Typical Struct and one Packed Struct. The Typical Struct is used in the code base and then, right before transmission, an instance of a Packed Struct is created and the fields from the Typical Struct copied over.

## Enums
[go back](#c)

All Enum names should be written in `lower_snake_case`. All Enum names should be prefixed meaningfully to avoid naming conflicts and end in `_fs_e` (for Flight Software Enum) to differentiate them from Structs. Member names should be be prefixed by a lowercase `e` and an `UPPER_SNAKE_CASE` version of the meaningful prefix. This is to further clarify that these constants are *not* macros. There are 3 parts in creating a new Enum:
- Defining the members
- Optionally choosing values
- Creating the typename

Defining the members and creating a typename are done as 2 steps in order to produce better error messages from compilers.

Example
```C
enum monitor_status_fs_e { // Step 1
    eMONITOR_STATUS_NOMINAL = 0, // Optional value
    eMONITOR_STATUS_ERR = 1 // Optional value
};
typedef enum monitor_status_fs_e monitor_status_fs_e; // Step 3
```
Optional values can be assigned to Enum variants if desired. However, if *any* field is given a specified value, then **all** fields must have a specified value, consider the following case to understand why:
```C
enum e {
    eA = 2,
    eB = 1,
    eC
};
printf("eA == eC: %s\n", eA == eC ? "true" : "false"); // prints "eA == eC: true"!
```

## Functions
[go back](#c)

All Functions are written with a `lower_snake_case` name. All Function names should be prefixed meaningfully to avoid naming conflicts.

All Functions must have a declaration in a Header File, and a definition in the matching Source File, regardless of the scope of their usage. The declarations in the Header File should be one after the other, no blank lines in-between. Function definitions in a Source File should be separated by a newline. Function definition braces are put on their own lines.

No function should ever need to be declared `extern` in this code base as it is useless when Functions are declared properly in Header Files. Function arguments follow the conventions of [Variables](#variables). 

Functions with no arguments should be written as taking 1 unnamed argument of type `void`, to ensure that compiler errors are generated if the Function is mistakenly called with any arguments.

The vast majority of Functions have a condition where they may fail. If this is the case, the function should follow the C convention of returning an `int` for its status. The return value is obtained via the macros `EXIT_SUCCESS == 0` and `EXIT_FAILURE == 1`, which are defined in `<stdlib.h>`.

When calling a function that has an `int` return value to indicate its status, you should always be checking the return value for errors and then responding appropriately. This should be done in a simple `if` statement that checks whether or not the function failed (returned a truthy value) or succeeded (returned a falsey value).

Example:
```C
// monitor.h
int monitor_append_datastream(uint8_t data_stream_id, uint8_t* data);
int do_thing(void); // No arguments
// monitor.c
int monitor_append_datastream(uint8_t data_stream_id, uint8_t* data)
{
    // do things
    if(function_that_may_fail(data_stream_id + 1)) return EXIT_FAILURE; // the function call failed so we should also fail
    return EXIT_FAILURE
}

int do_thing(void)
{
    return EXIT_SUCCESS;
}
```

There are several subcases, that may be mixed-and-matched:
1. [Typical Functions](#typical-functions)
2. [Static Functions](#static-functions)
3. [Inline Functions](#inline-functions)

### Typical Functions
[go back](#functions)

Typical functions are written as in the example in the [Functions](#functions) section.

### Static Functions
[go back](#functions)

Static Functions have an additional keyword, `static` written before the return type of the Function in both the Header and Source Files. Static Functions should be used when you want to have a Function that is "private" to the Header/Source File pair that is declared and defined in. Static Functions should be grouped in the Header file.

Example:
```C
// monitor.h
static int setup_hmd(void);
// monitor.c
static int setup_hmd(void)
{
    return EXIT_SUCCESS;
}
```

### Inline Functions
[go back](#functions)

Inline functions have an additional keyword, `inline` written before the return type (and after `static`, if present) of the Function in both the Header and Source Files. Inline Functions are used when you want to indicate to the compiler that the Function is simple enough that it is a reasonable optimization to insert the Function's code directly into its call sites, rather than generating a function call. Note that the compiler will often ignore an `inline` request if it does not seem reasonable.

Example:
```C
// monitor.h
static inline double quick_calculation(void);
// monitor.c
static inline double quick_calculation(void)
{
    return 1.0 + 2.0;
}
```

## Variables
[go back](#c)

Variables 

## Header Files
[go back](#c)

Header files are written with a `.h` extension. There should be a logical mapping from 1 `.h` header file to 1 `.c` source file for programs, library header files can exist on their own. Header files consist of the following sections **in this order**:
1. [Beginning Include Guards](#beginning-include-guards)
2. [System Headers](#system-headers)
3. [Project Headers](#project-headers)
4. [Publicly Visible Macros](#publicly-visible-macros)
5. [Publicly Visible Type Definitions](#publicly-visible-type-definitions)
6. [Publicly Visible Functions for Internal and/or External Usage](#publicly-visible-functions-for-internal-and-or-external-usage)
7. [Publicly Visible Functions for Internal Usage Only](#publicly-visible-functions-for-internal-usage-only)
8. [Ending Include Guards](#ending-include-guards)

### Beginning Include Guards
Example Path | Project Program Header | Project Library Header
:--- | :---: | ---:
project_root/src/hm_d/monitor.h | `_SRC_HMD_MONITOR_H_` | N/A
project_root/lib/libudp.h | N/A | `_LIB_LIBUDP_H_`

Beginning Include Guards consists of 2 macros, `#ifndef` and `#define`. The first is used to ensure that the coming macro has not been seen before. The seconds defines the macro to prevent double inclusion issues.

Example:

```C
#ifndef _SRC_HMD_MONITOR_H_
#define _SRC_HMD_MONITOR_H_

// ...
```

### System Headers
System Headers are anything provided by the C library, Linux, etc... (stuff you would use `<header.h>` for).

System Headers should be grouped by folder where applicable, e.g. `sys/` or `linux/`, should be grouped together.

There is no other requirements on ordering. No comments are necessary on system header inclusions.

Example:
```C
#include <stdint.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <unistd.h>
```

### Project Headers
Project Headers are all the headers that aren't System Headers, (so stuff we've written or imported from a 3rd-party library).

A single blank line should seperate Project Headers from the proceeding System Headers.

Project Headers should include the path to the header file, relative to the project root **except** if the header is already in the same directory. Include files should be grouped by their parent folder. No comments are necessary.

The order of includes goes:
1. Library Headers
2. Project Headers Outside Current Dirctory
3. Project Headers Inside Current Directory

There are two definitions to consider for Project Headers:

1. Private Headers are any `.h` files for Libraries or Programs that are **not** to be included *directly* by external (outside of the current directory) components. They can include any desired Project Headers.

2. Public Headers are any `.h` files for Libraries or Programs that are meant to be included by external components. They should include any Private Headers that may be useful to external components. There should only be a single Public Header per Library or Source directory.

Example from a Project Source:
```C
/*
 * Assume project structure like:
 * project_root/src/hm_d/monitor.h
 * project_root/src/hm_d/monitor_other.h
 * project_root/src/bp_d/powerboard.h // Includes several other powerboard_*.h headers
 * project_root/lib/libudp.h
 * project_root/lib/librpc.h
 * this file is: project_root/src/hm_d/monitor.h
 */
// System Headers...

#include "lib/libudp.h"
#include "lib/librpc.h"
#include "src/bp_d/powerboard.h"
#include "monitor_other.h"
```



### Publicly Visible Type Definitions

### Publicly Visible Functions for Internal and/or External Usage

### Publicly Visible Functions for Internal Usage Only

### Ending Include Guards
